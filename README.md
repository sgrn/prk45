# Python implementation of the Runge Kutta-Fehlberg method.

The project is a simple implementation of the Runge Kutta-Fehlberg. It approximates the solutions of any reduced n-order differential equations, given a set of initial conditions. It then plots the result using the matplotlib library. The step is adaptive, meaning it will change depending on both the local error at any x position and a given error tolerance interval.

## Run

This project requires matplotlib to graph the approximated solutions.

### Install matplotlib

Use pip, the python package manager to install matplotlib.

	 sudo pip3 install matplotlib 

If this command does not work out for you, go to the matplotlib [documentation](https://matplotlib.org/stable/users/installing.html).

### Execution and imports

The *rk45.py* is a module that is importable in any of your projects. The *pendulum.py* is a demonstration file in which the differential equations of the simple pendulum are both solved and graphed using the *rk45.py* module. Check out this file closely to determine how to use the *rk45.py* module. You can also read the docstrings present for each function in the *rk45.py* file.

## Reducing equations.

Examples of reduced equations are present in the *pendulum.py* file.

# Plotting examples

![rk45 approximation of pendulum general equation](Img/pendulum-general-solution.png)
![phase diagram for pendulum general equation](Img/pendulum-phase-diagram-general-eq.png)
![rk45 approximation of pendulum frictionless equation](Img/pendulum-solution-frictionless.png)
![phase diagram for pendulum frictionless equation](Img/pendulum-phase-diagram-frictionless.png)

*for any other request, contact the author at <sangsoic@protonmail.com>*
