#! /usr/bin/python3

from rk45 import *

# Constants

k = 0.094  # friction coefficient.
m = 0.470  # Mass.
g = 9.81   # Gravitational acceleration.
l = 0.30   # String length.

theta = '\N{GREEK SMALL LETTER THETA}'

# DEQ 0 : theta'' + k/m * theta' + g/l * sin(theta) = 0, general form, reduced definition.

DEQ0 = (
    lambda t, theta, Theta : Theta,                               # theta'  = Theta
    lambda t, theta, Theta : -(k/m * Theta + g/l * np.sin(theta)) # theta'' = -(k/m * Theta + g/l * np.sin(theta))
)

# DEQ 1 : theta'' + g/l * sin(theta) = 0, frictionless form, reduced definition.

DEQ1 = (
    lambda t, theta, Theta : Theta,               # theta'  = Theta
    lambda t, theta, Theta : -g/l * np.sin(theta) # theta'' = -g/l * np.sin(theta)
)

# DEQ 2 : theta'' + k/m * theta' + g/l * theta = 0, small oscillations, reduced definition.

DEQ2 = (
    lambda t, theta, Theta : Theta,                       # theta'  = Theta
    lambda t, theta, Theta : -(k/m * Theta + g/l * theta) # theta'' = -(k/m * Theta + g/l * theta)
)

# DEQ 3 : theta'' + g/l * theta = 0, small oscillations frictionless, reduced definition.

DEQ3 = (
    lambda t, theta, Theta : Theta,       # theta'  = Theta
    lambda t, theta, Theta : -g/l * theta # theta'' = -g/l * theta
)

# Settings for large angles -------------------

t0 = 0
tf = 20
initialAngle = np.pi/4
initialSpeed = 0

initalStep = 0.01
stepMin    = 0.001
stepMax    = 0.01

errorMin = 0.001
errorMax = 0.1

# Large angles with friction

Xs, Ys = approximate_DEq_using_rk45(DEQ0, initalStep, stepMin, stepMax, errorMin, errorMax, tf, t0, initialAngle, initialSpeed)

plot(Xs, Ys, "time (s)", "", "General: %c'' + k/m * %c' + g/l * sin(%c) = 0" % (theta, theta, theta), theta, "rad", "rad/s")

plot([ys[0] for ys in Ys], [[ys[1]] for ys in Ys], "angle (rad)", "velocity (rad/s)", "Phase diagram for general equation: %c'' + k/m * %c' + g/l * sin(%c) = 0" % (theta, theta, theta), theta+'\'', "rad/s")

# Large angles without friction

Xs, Ys = approximate_DEq_using_rk45(DEQ1, initalStep, stepMin, stepMax, errorMin, errorMax, tf, t0, initialAngle, initialSpeed)

plot(Xs, Ys, "time (s)", "", "Friction Less: %c'' + g/l * sin(%c) = 0" % (theta, theta), theta, "rad", "rad/s")

plot([ys[0] for ys in Ys], [[ys[1]] for ys in Ys], "angle (rad)", "velocity (rad/s)", "Phase diagram for frictionless equation: %c'' + g/l * sin(%c) = 0" % (theta, theta), theta+'\'', "rad/s")

# Settings for small angles --------------------

t0 = 0
tf = 20
initialAngle = np.pi/36
initialSpeed = 0

initalStep = 0.01
stepMin    = 0.001
stepMax    = 0.01

errorMin = 0.001
errorMax = 0.1

# Small angles with friction

Xs, Ys = approximate_DEq_using_rk45(DEQ2, initalStep, stepMin, stepMax, errorMin, errorMax, tf, t0, initialAngle, initialSpeed)

plot(Xs, Ys, "time (s)", "", "Small angles equation: %c'' + k/m * %c' + g/l * %c = 0" % (theta, theta, theta), theta, "rad", "rad/s")

plot([ys[0] for ys in Ys], [[ys[1]] for ys in Ys], "angle (rad)", "velocity (rad/s)", "Phase diagram for small angles equation: %c'' + k/m * %c' + g/l * %c = 0" % (theta, theta, theta), theta+'\'', "rad/s")

# Small angles without friction

Xs, Ys = approximate_DEq_using_rk45(DEQ3, initalStep, stepMin, stepMax, errorMin, errorMax, tf, t0, initialAngle, initialSpeed)

plot(Xs, Ys, "time (s)", "", "Small angles and frictionless equation: %c'' + g/l * %c = 0" % (theta, theta), theta, "rad", "rad/s")

plot([ys[0] for ys in Ys], [[ys[1]] for ys in Ys], "angle (rad)", "velocity (rad/s)", "Phase diagram for small angles and frictionless equation: %c'' + g/l * %c = 0" % (theta, theta), theta+'\'', "rad/s")

plt.show()
