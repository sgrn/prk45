#! /usr/bin/python3
"""
Copyright 2021 Sangsoic

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import numpy as np
from matplotlib import pyplot as plt


"""
   next_ys_using_rk45(slopeFields, step, xi, yi, *higherOrderNaughts)

Calculates the next y y' ... y^(n-1) at xi+step, given : an n-order differential equation, xi, and y y' ... y^(n-1) at xi.

AUTHOR : Sangsoic <sangsoic@protonmail.com>

Parameters
----------
slopeFields : tuple of callable lambda functions
    Reduced differential equation
step : numerical value
    x span between the previously approximated point and the one to approximate
xi : numerical value
    x value of the previously approximated point
yi : numerical value
    y value at xi
higherOrderNaughts : tuple of numerical values (optional parameters)
    y' value at xi
    y'' value at xi
    ...
    y^(n-1) value at xi

Returns
-------
rk4yis : list of numerical values
    list of y y' ... y^(n-1) at xi+step approximated using a 4th order accurate method
rk5yis : list of numerical values
    list of y y' ... y^(n-1) at xi+step approximated using a 5th order accurate method

Notes
-----
- Implementation of the Runge Kutta order 4(5) method for n-order differential equations.
- The number of required naughts depends on the order of the given differential equation.
    Ex : For n >= 1 integer, an n-order differential equation will require n-1 naughts.

"""
def next_ys_using_rk45(slopeFields, step, xi, yi, *higherOrderNaughts) :
    yNaughts = (yi,) + higherOrderNaughts

    args1    = (xi, yi) + higherOrderNaughts
    slopes1  = [slopeField( *args1 ) for slopeField in slopeFields]

    args2    = [xi + step/4] + [naught + step/4 * slope1 \
                    for naught, slope1 in zip(yNaughts, slopes1)]
    slopes2  = [slopeField( *args2 ) for slopeField in slopeFields]

    args3    = [xi + step*3/8] + [naught + step/32*(3*slope1 + 9*slope2) \
                    for naught, slope1, slope2 in zip(yNaughts, slopes1, slopes2)] 
    slopes3  = [slopeField( *args3 ) for slopeField in slopeFields]

    args4    = [xi + step*12/13] + [naught + step/2197*(1932*slope1 - 7200*slope2 + 7296*slope3) \
                    for naught, slope1, slope2, slope3 in zip(yNaughts, slopes1, slopes2, slopes3)]
    slopes4  = [slopeField( *args4 ) for slopeField in slopeFields]

    args5    = [xi + step] + [naught + step*(439/216*slope1 - 8*slope2 + 3680/513*slope3 - 845/4104*slope4) \
                    for naught, slope1, slope2, slope3, slope4 in zip(yNaughts, slopes1, slopes2, slopes3, slopes4)]
    slopes5  = [slopeField( *args5 ) for slopeField in slopeFields]

    rk4yis   = [naught + step*(25/216*slope1 + 1408/2565*slope3 + 2197/4104*slope4 - slope5/5) \
                    for naught, slope1, slope3, slope4, slope5 in zip(yNaughts, slopes1, slopes3, slopes4, slopes5)]

    args6    = [xi + step/2] + [naught + step*(-8/27*slope1 + 2*slope2 - 3544/2565*slope3 + 1859/4104*slope4 - 11/40*slope5) \
                    for naught, slope1, slope2, slope3, slope4, slope5 in zip(yNaughts, slopes1, slopes2, slopes3, slopes4, slopes5)]
    slopes6  = [slopeField( *args6 ) for slopeField in slopeFields]

    rk5yis   = [naught + step*(16/135*slope1 + 6656/12825*slope3 + 28561/56430*slope4 - 9/50*slope5 + 2/55*slope6) \
                    for naught, slope1, slope3, slope4, slope5, slope6 in zip(yNaughts, slopes1, slopes3, slopes4, slopes5, slopes6)]

    return rk4yis, rk5yis

"""
   approximate_DEq_using_rk45(differentialEquation, step0, stepMin, stepMax, errorMin, errorMax, xFinal, x0, y0, *higherOrderNaughts)

Approximates y y' ... y^(n-1) solutions over [x0, xFinal[, for an n-order differential equation given initial conditions x0 and y, y' ... y^(n-1) at x0
    with a step adapting within [stepMin, stepMax] based on an error tolerance interval [errorMin, errorMax].

AUTHOR : Sangsoic <sangsoic@protonmail.com>

Parameters
----------
slopeFields : tuple of callable lambda functions
    Reduced differential equation
step0 : numerical value
    initial step included within [stepMin, stepMax]
stepMin : numerical value
    maximum step
stepMax : numerical value
    minimum step
errorMin : numerical value
    minimum error
errorMax : numerical value
    maximum error
xFinal : numerical value
    sup of the interval over which the solutions are approximated.
x0 : numerical value
    x value of the initial point from which to start the approximation
y0 : numerical value
    y value at x0

higherOrderNaughts : tuple of numerical values (optional parameters)
    y' value at x0
    y'' value at x0
    ...
    y^(n-1) value at x0

Returns
-------
Xs : list of numerical values
    list of x values at which the solutions were approximated
Ys : list of list of numerical values
    [[y at x0, y' at x0, ..., y^(n-1) at x0],
     [y at x1, y' at x1, ..., y^(n-1) at x1],
     ...
     [y at xf, y' at xf, ..., y^(n-1) at xf]]
    with : xf being an integer such that xf < xFinal <= xf + lastStep

Notes
-----
- Implementation of the Runge Kutta order 4(5) method for n-order differential equations.
- The number of required naughts depends on the order of the given differential equation.
    Ex : For n >= 1 integer, an n-order differential equation will require n-1 naughts.
- The error is the absolute value of the difference between what the 4th order method yields and what the 5th order method yields.
    The local error is the error at each point, while the global error is the accumulation of all local errors over an interval.

"""
def approximate_DEq_using_rk45(differentialEquation, step0, stepMin, stepMax, errorMin, errorMax, xFinal, x0, y0, *higherOrderNaughts) :
    Xs    = []
    Ys    = []
    step  = step0
    xi    = x0
    yis   = (y0,) + higherOrderNaughts
    if step < stepMin :
        step = stepMin
    elif step > stepMax :
        step = stepMax
    while xi < xFinal :
        Xs.append(xi)
        Ys.append(yis)
        rk4yis, rk5yis = next_ys_using_rk45(differentialEquation, step, xi, *yis)
        error = abs(rk4yis[0] - rk5yis[0])
        while error > errorMax and step/2 > stepMin:
            step /= 2
            rk4yis, rk5yis = next_ys_using_rk45(differentialEquation, step, xi, *yis)
            error = abs(rk4yis[0] - rk5yis[0])
        yis = rk5yis
        if error < errorMin and step*2 < stepMax:
            step *= 2
        xi += step
    return Xs, Ys

"""
   plot(Xs, Ys, xlabel, ylabel, title, functionName, *units)

Plots the results of the 'approximate_DEq_using_rk45' function

AUTHOR : Sangsoic <sangsoic@protonmail.com>

Parameters
----------
Xs : list of numerical values
    list of x values at which the solutions were approximated
Ys : list of list of numerical values
    [[y at x0, y' at x0, ..., y^(n-1) at x0],
     [y at x1, y' at x1, ..., y^(n-1) at x1],
     ...
     [y at xf, y' at xf, ..., y^(n-1) at xf]]
    with : xf being an integer such that xf < xFinal <= xf + lastStep
           n being the order of  the differential equation
xlabel : string
    x axis label
ylabel : string
    y axis label
title : string
    axes title
functionName : string
    name of the function to plot
units : tuple of strings (optional parameter)
    units of the function and its derivatives

"""
def plot(Xs, Ys, xlabel, ylabel, title, functionName, *units) :
    fig, ax = plt.subplots()
    order = len(Ys[0])
    rotatedYs = [[] for j in range(order)]
    for yis in Ys :
        for j, yi in zip(range(order), yis) :
            rotatedYs[j].append(yi)
    for n, yis in zip(range(order), rotatedYs) :
        ax.plot(Xs, yis, label="%s%s (%s)" % (functionName, n*"'", units[n]))
    ax.legend()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.grid(True)
